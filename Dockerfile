# Based on Google Puppeteer's recommended Dockerfile, now only a historical version:
# https://github.com/puppeteer/puppeteer/blob/v3.0.1/.ci/node12/Dockerfile.linux
# with updates from https://pptr.dev/troubleshooting/#chrome-headless-doesnt-launch-on-unix

ARG IMAGE_TAG=lts

FROM node:$IMAGE_TAG

# Ignore rule to pin package versions.  Given the number of dependencies this isn't feasible,
# and want the the latest to keep up with Chromium/Puppeteer changes.
# hadolint ignore=DL3008
RUN apt-get update && \
  apt-get -y install --no-install-recommends ca-certificates fonts-liberation libasound2 \
  libatk-bridge2.0-0 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 \
  libfontconfig1 libgbm1 libgcc1 libglib2.0-0 libgtk-3-0 libnspr4 libnss3 libpango-1.0-0 \
  libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 \
  libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxshmfence1 libxss1 \
  libxtst6 lsb-release procps wget xdg-utils && \
  rm -rf /var/lib/apt/lists/*

# Add user to avoid --no-sandbox (recommended)
RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
  && mkdir -p /home/pptruser/Downloads \
  && chown -R pptruser:pptruser /home/pptruser

# Run everything after as non-privileged user.
USER pptruser

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-puppeteer"
LABEL org.opencontainers.image.title="Docker Puppeteer"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-puppeteer"
