# Docker Puppeteer

> DEPRECATION NOTICE:
>
> With v13.0.0, this project has been moved to
> <https://gitlab.com/gitlab-ci-utils/container-images/puppeteer> for further
> development, including updating to Debian Bookworm and support for any future
> Node major releases. This repository will remain only to continue support for
> Node 18, 20, and 22 on Debian Bullseye, which will continue to be updated
> for new Node minor/patch releases for those Node versions. Container images
> will be removed at version end of life per the policy outlined
> [here](#docker-puppeteer-container-images).
>
> Migration to the new project is advised as soon as practical, but note that
> given the GitLab container registry implementation the container image name
> has changed with the project move.
>
> Once Node 22 is end of life, this project will be archived.
> If GitLab completes the [image pull count](https://gitlab.com/groups/gitlab-org/-/epics/14021)
> feature and data shows no active use before then, the project may be
> archived earlier.

## About Docker Puppeteer

Docker Puppeteer is a container image with the recommended configuration for
using [Puppeteer](https://github.com/GoogleChrome/puppeteer) to drive a
headless Chrome browser.

**Notes:**

- This image doesn't install Puppeteer itself. It's assumed Puppeteer is
  installed by the appropriate application using the container.
- This image runs under a non-privileged account, so Puppeteer may fail to
  install globally (see <https://github.com/GoogleChrome/puppeteer/issues/375>
  for details and workarounds).
- In order for Chrome to launch properly in this container in some
  configurations, the `--no-sandbox` Puppeteer argument may be required.
- As of v9.0.0, these images are based on the Node Debian Slim images. Details
  on the differences can be found [here](docs/debain-vs-debian-slim.md).

### Differences from the Puppeteer Dockerfile

Puppeteer v16 added an
["official" Dockerfile](https://github.com/puppeteer/puppeteer/blob/main/docker/Dockerfile),
which is a different configuration than this image. That Dockerfile installs
the latest stable Chrome, which is used to install the majority of the Chrome
dependencies, as well as the latest Puppeteer. While this creates a container
image compatible with that version of Puppeteer, it may not necessarily be
compatible with other versions that have different OS dependency requirements.
This image is based on the
[core Debian dependencies](https://pptr.dev/troubleshooting/#chrome-headless-doesnt-launch-on-unix)
with [a few other recommendations](https://github.com/puppeteer/puppeteer/issues/7822)
to try to maintain compatibility across a spectrum of Node, Debian, and
Puppeteer versions. It's currently tested against the latest Puppeteer release
as well v9.1.1 (since still used by [Pa11y](https://github.com/pa11y/pa11y),
which is the test case as well as one of the core use cases for this image).

## Docker Puppeteer container images

All available Docker image tags can be found in the
`gitlab-ci-utils/docker-puppeteer` repository at
<https://gitlab.com/gitlab-ci-utils/docker-puppeteer/container_registry>. Details
on each release can be found on the
[Releases](https://gitlab.com/gitlab-ci-utils/docker-puppeteer/releases) page.

The following tag conventions are used:

- `latest`: Based on the current Node active LTS release (currently Node 18).
  This is rebuilt on any update to the most recent Node base image for the
  active LTS release.
- `node-xx`: Based on the most recent build of the Node major version `xx`
  image for all currently supported Node major versions - 16 (`node-16`), 18
  (`node-18`), 20 (`node-20`). These are rebuilt on any update to the most
  recent Node base image for that major version (due to Node release or base
  Debian image).
- `node-xx.yy.zz-os-slim`: Based on the most recent build of Node image tagged
  `xx.yy.zz-os-slim` for all currently supported Node major versions - 16 (for
  example `16.20.0-buster-slim`), 18 (for example `18.16.0-buster-slim`), 20
  (for example `20.2.0-bullseye-slim`). These are rebuilt on any updates for
  that Node image tag (which are rebuilt based on changes to the underlying
  Debian image).

**Note: images for each Node major version are maintained while that version is
supported per the
[Node Release Working Group schedule](https://github.com/nodejs/Release). Once
end-of-life, images for that major release are kept for a month, at which point
they may be removed from the registry.**

**Note:** Any images in the `gitlab-ci-utils/docker-puppeteer/tmp` repository
are temporary images used during the build process and may be deleted at any
point.
