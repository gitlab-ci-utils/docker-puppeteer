#!/bin/sh

# Archive all container images for a given major Node.js version
# from this project's GitLab container registry.

# Designed to be run in a base Alpine image, so install required dependencies.
apk update && apk add jq skopeo

# The GitLab project and repository IDs (gitlab-ci-utils/docker-puppeteer,
# the default container registry).
project_id=13728947
repository_id=1824553
# The GitLab API URL to get all tags for the repository.
tags_url="https://gitlab.com/api/v4/projects/${project_id}/registry/repositories/${repository_id}/tags?per_page=100"
# The image name and tags to archive
# (i.e. "<image_name>:<tag_prefix><major_version>*").
image_name="registry.gitlab.com/gitlab-ci-utils/docker-puppeteer"
tag_prefix="node-"
major_version=21
# The folder to archive the images to.
archive_folder="./Archive"

# Get all tags that match the major version (e.g. node-21,
# node-21.7.3-bullseye-slim). No credentials are required since this is a
# public repository.
tags=$(wget -qO- "$tags_url" | jq -r '.[].name' | grep "^${tag_prefix}${major_version}")
for tag in $tags; do
  # Get the file name from image/tag, copy to a local Docker
  # archive file, and gzip.
  image_tag="${image_name}:${tag}"
  filename=$(echo "${image_tag}.tar" | sed 's/[\/:]/_/g')
  filepath="${archive_folder}/${filename}"

  echo "Archiving ${image_tag} to ${filepath}"
  skopeo copy docker://"$image_tag" docker-archive:"$filepath"
  printf "Compressing %s\n\n" "${filepath}"
  gzip "$filepath"
done

printf "Archived images:\n"
find "${archive_folder}" -type f -name "*${tag_prefix}${major_version}*" -exec du -h {} \;
